#include <bits/stdc++.h>

using namespace std;

long long  n;
map<long long, bool> a;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    cin >> n;
    while(!a[1]){
        cout << n << " ";
        a[n] = true;
        if(n%2==0)
            n/=2;
        else
            n=n*3+1;
    }
    return 0;    
}