/**
 * author:  hovananhk58a2
**/

#include<bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/tree_policy.hpp>
 
using namespace std;
using namespace __gnu_pbds;
 
#define ar array
#define vt vector
#define all(v) (v).begin(), (v).end()
#define pb push_back
#define mp make_pair
#define ll long long
#define ld long double
#define pii pair<int, int>
#define piii pair<int, ii>
#define vc vt<char>
#define vi vt<int>
#define vl vt<ll>
#define fi firsts
#define se second
#define FORIT(i, s) for (auto it=(s.begin()); it!=(s.end()); ++it)
#define FOR(i, a, b, s) for (int i=(a); (s)>0? (unsigned)i<=(b) : (unsigned)i>=(b); i+=(s))
#define FORD(n) FOR(i, 1, n, 1)
#define FORE(i, e) FOR(i, 1, e, 1)
#define FORF(i, b, e) FOR(i, b, e, 1)
#define FORG(i, b, e, s) FOR(i, b, e, s)
#define FORK(...) GET5(__VA_ARGS__, FORG, FORF, FORE, FORD)
#define FORH(...) FORK(__VA_ARGS__)(__VA_ARGS__)
#define GET5(a, b, c, d, e, ...) e
#define EACH(x, a) for(auto& x: a)
#define BUG(x){cout << #x << " = " << x << "\n";}
#define oo LLONG_MAX
 
const int d4x[] = {-1, 0, 1, 0},
          d4y[] = {0, -1, 0, 1},
          d8x[] = {-1, -1, -1, 0, 0, 1, 1, 1},
          d8y[] = {-1, 0, 1, -1, 1, -1, 0, 1};
 
template<class T1, class T2> ostream &operator <<(ostream &cout, pair<T1, T2> x){
    cout << "(" << x.fi << "," << x.se << ")";
    return cout;
}
 
template<class T> void print1(T &x){
    EACH(xi, x) cout << xi << ' ';
    cout << '\n';
}
 
template<class T> void print2(vt<T> &x){
    FORH(x.size()){
        print1(x[i]);
    }
}

/**
 * Code starts here
 **/

string s;
priority_queue<int> a;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    // freopen("test.inp", "r", stdin);
    // freopen("test.out", "w", stdout);

    cin >> s;
    int d = 1;
    FORF(i, 1, s.size()-1){
        if(s[i] == s[i-1]){
            d++;
        }else{
            a.push(d);
            d = 1;
        }
    }
    a.push(d);
    cout << a.top();

    return 0;
}